// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Cancel the default action, if needed
        event.preventDefault();
        // Trigger the button element with a click
        document.getElementById("myBtn").click();
    }
});

function movieRecommend(){
    // Get the input field
    var title = document.getElementById("movieTitle").value;
    var year = document.getElementById("movieYear").value;
    title = title.toLowerCase().replace(" ", "_");
    var json_obj = JSON.parse(Get('https://yrdymze5bh.execute-api.ca-central-1.amazonaws.com/Prod/recommend?title='
        + title + '&year=' + year));
    document.getElementById("movie1").textContent = json_obj[0];
    document.getElementById("movie2").textContent = json_obj[1];
    document.getElementById("movie3").textContent = json_obj[2];
    document.getElementById("movie4").textContent = json_obj[3];
    document.getElementById("movie5").textContent = json_obj[4];
    document.getElementById("movie6").textContent = json_obj[5];
    document.getElementById("movie7").textContent = json_obj[6];
    document.getElementById("movie8").textContent = json_obj[7];
    document.getElementById("movie9").textContent = json_obj[8];
    document.getElementById("movie10").textContent = json_obj[9];
}

function Get(Url){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",Url,false);
    Httpreq.send(null);
    return Httpreq.responseText;
}