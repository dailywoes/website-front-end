$(document).ready(function () {
    $("#movie-form").validate({
        rules: {
            movieTitle: {
                required: true,
                minlength: 2
            },
            movieYear: {
                required: true,
                number: true,
                lessThan: "2020"
            }
        },
        messages: {
            movieTitle: {
                required: "Please enter the movie title.",
                minlength: "Name should be at least 2 characters."
            },
            movieYear: {
                required: "Please enter the movie release date.",
                number: "Please enter a number for the release date.",
                lessThan: "You must enter a year before 2020."
            }
        }
    });

    $("#movie-form").submit(function(e) {

        var message="<p class='lead align-center' id='movie-form-submitted'>Please wait...</p>"
        var message2="<p class='lead align-center' id='movie-form-submitted'>Your top 10 movies:</p>"
        if($(this).valid()) {
            e.preventDefault();
            $("#movie-form-submitted").replaceWith(message);
            function recommend(){
                movieRecommend();
                $("#movie-form-submitted").replaceWith(message2);
            };
            window.setTimeout(recommend,100);
        }
    });
});

$.validator.addMethod("lessThan",
    function (value, element) {
        return parseInt(value, 10) < 2020;
    });