$(document).ready(function () {
    $("#contact-form").validate({
        rules: {
            contactName: {
                required: true,
                minlength: 2
            },
            contactEmail: {
                required: true,
                minlength: 2
            },
            contactSubject: {
                required: true,
                minlength: 2
            },
            contactMessage: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            contactName: {
                required: "Please enter your name.",
                minlength: "Name should be at least 2 characters."
            },
            contactEmail: {
                required: "Please enter your email.",
                minlength: "Email should be at least 2 characters."
            }
        }
    });

    $("#contact-form").submit(function(e) {
        var message="<h5 class='lead align-center' id='pendingMessage'>Email sent!</h5>"
        if($(this).valid()) {
            e.preventDefault();
            $("#contact-form-submitted").replaceWith(message);
        }
    });
});